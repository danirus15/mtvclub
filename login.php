<!DOCTYPE html>
<html>
<?php $page="";?>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/framebar.php";?>
	<div class="skin1">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="skin2">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="container">
		<!-- s:header -->
		<?php include "includes/header.php";?>
		<!-- e:header -->
		<div class="content">
			<!-- s:title page -->
			<div class="title_page">
				<span>login</span>
			</div>
			<!-- e:title page -->
			<!-- s:left	 -->
			<div class="c_left">
				<div class="login_box">
					<h2>SIGN-IN ACCOUNT</h2>
					<form action="" class="form_login">
						<div class="t1">Email</div>
						<div class="t2">
							<input type="text" class="input" placeholder="Email Account">
						</div>
						<div class="clearfix pt15"></div>
						<div class="t1">Password</div>
						<div class="t2">
							<input type="password" class="input" placeholder="Password">
						</div>
						<div class="clearfix pt15"></div>
						<div class="t1">&nbsp;</div>
						<div class="t2"><input type="submit" value="login" class="btn"></div>
						<div class="clearfix"></div>
					</form>
					<div class="clearfix pt20"></div>
					<div class="or">OR</div>
					<div class="clearfix pt20"></div>
					<a href="#" class="mr15"><img src="img/s_tw.png" alt=""></a>
					<a href="#"><img src="img/s_fb.png" alt=""></a>

				</div>
			</div>
			<!-- e:left	 -->
			<!-- s:right	 -->
			<div class="c_right">
				<!-- s:scratch -->
				<div class="box_1">
					<div class="title">
						<span>SCRATCH</span>
					</div>
					<ul class="list_musik">
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
					</ul>
				</div>
				<!-- e:scratch -->
				<!-- s:banner r1 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- e:banner r1 -->
				<!-- s:quiz -->
				<div class="box_1 box_2">
					<div class="title">
						<span>quiz</span>
					</div>
					<div class="quiz_box">
						<form action="#">
							<div class="q">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat, magna vitae porta scelerisque ?
							</div>
							<div class="a">
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare. 
									</div>
									<div class="clearfix"></div>
								</label>
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare consectetur adipiscing elit.  
									</div>
									<div class="clearfix"></div>
								</label>
							</div>
							<input type="submit" value="SUBMIT" class="btn_submit">
						</form>
					</div>
				</div>
				<!-- e:quiz -->
				<!-- s:banner r2 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- s:banner r2 -->
			</div>
			<!-- s:right	 -->
			<div class="clearfix"></div>
		</div>
		<!-- s:footer -->
		<?php include "includes/footer.php";?>
		<!-- e:footer -->
	</div>
	<?php include "includes/js.php";?>
</body>
</html>