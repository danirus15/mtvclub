$(document).ready(function() {
  $('#nav_menu').click(function() {
    $('#nav').slideDown(200);
    $('#nav_menu').hide();
    $('#nav_menu_close').show();
  });
  $('#nav_menu_close').click(function() {
    $('#nav').slideUp(200);
    $('#nav_menu').show();
    $('#nav_menu_close').hide();
  });

  $(".imgLiquid").imgLiquid({fill:true});

});

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});

$(function() {
  $('#hl').carouFredSel({
    responsive: true,
    pagination  : "#pagnation",
     auto:true,
    items: {
      visible: {
        min: 1,
        max: 1
      }
    },
    scroll: {
      duration:700
    },
    swipe: {
                  onMouse: true,
                  onTouch: true
              }
  });

  $('#thumbs').carouFredSel({
        /*responsive: true,*/
        synchronise: ['#images', false, true],
        auto: false,
        items: {
            visible: 8,
            start: -1
        },
        scroll: {
            onBefore: function( data ) {
                data.items.old.eq(1).removeClass('selected');
                data.items.visible.eq(1).addClass('selected');
            },
            items:1
        },
        prev: '#prev',
        next: '#next'
    });

    $('#images').carouFredSel({
        responsive: true,
        auto: false,
        items: 1,
        scroll: {
            fx: 'fade',
            items:1,
            onBefore : function( data ) {
                data.items.visible.find(".text").hide();
            },
            onAfter : function( data ) { 
                data.items.visible.find(".text").fadeIn(200);
            }
        }
        
    });

    $('#thumbs div').click(function() {
        $('#thumbs').trigger( 'slideTo', [this, -1] );
    });
    $('#thumbs div:eq(1)').addClass('selected');
});


/*S:MODALBOX*/
if( $('div.pop_box').attr('id') == 'pop_box_now'){
  $("body").css('overflow','hidden');
}
else {
  $("body").css('overflow','scroll');
}

$(".box_modal").click(function (){
  if( $('div').attr('id') == 'pop_box_now'){}
  else{ 
    $(this).removeAttr('href');
    var src  = $(this).attr("alt");
    size   = src.split('|');
        url      = size[0],
        width    = '100%',
        height   = '100%'

    $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('position','fixed').css('top','0').css('left','0');
    $("body").css('overflow','hidden');
  }
});
$(".box_modal2").click(function (){
  $("#pop_box2").show();
  $("body").css('overflow','hidden');
});
$(".box_modal3").click(function (){
  $("#pop_box3").show();
  $("#pop_box2").hide();
  $("body").css('overflow','hidden');
});
$(".box_modal4").click(function (){
  $("#pop_box4").show();
  $("#pop_box3").hide();
  $("#pop_box2").hide();
  $("body").css('overflow','hidden');
});

function closepop() { 
  $("#pop_box_now").remove();
  $("#pop_box2").hide();
  $("#pop_box3").hide();
  $("body").css('overflow','scroll');
};
$(".close_box").click(function (){
  closepop();
});
$(".close_box_in").click(function (){
  parent.closepop();
});
/*S:MODALBOX*/

