<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
<link rel="shortcut icon" href="img/favicon.png">
<link rel="apple-touch-icon" href="img/favicon.png"/> 
<title>MTV Club</title>
<link href="css/fonts.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/frame.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/style.responsive.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<script type="text/javascript" src="js/jquery.js"></script>
</head>