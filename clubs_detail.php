<!DOCTYPE html>
<html>
<?php $page="clubs";?>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/framebar.php";?>
	<div class="skin1">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="skin2">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="container">
		<!-- s:header -->
		<?php include "includes/header.php";?>
		<!-- e:header -->
		<div class="content">
			<!-- s:title page -->
			<div class="breadcrumb">
				<span>
					<a href="#">HOME</a> /
					<a href="#">clubs</a>
				</span>
			</div>
			<!-- e:title page -->
			<!-- s:left	 -->
			<div class="c_left detail">
				<article>
					<!-- s:title2 -->
					<div class="snap_title">
						<h1>green valley</h1>
						oleh : Anita K Wardhani  /  Jumat, 28 November 2014
					</div>
					<!-- e:title2 -->

					<!-- s:foto -->
					<div class="pic_detail">
						<img src="img/hl2.jpg" alt="">
					</div>
					<!-- e:foto -->
					<!-- s:text detail -->
					<div class="text_detail">
						Disk Jockey Winky Wiryaman meluncurkan satu album yang berisi dua kompiliasi CD. Proses pembuatan CD yang terdiri 17 lagu itu dibantu sejumlah musisi. Di antaranya Abdurrachman Arif, Aqinomoto Singgih, Audrey Singgih, Evan Virgin, Ivandeva, Maruli Tampubolon, dan Tatisboom.
						<br><br>
						"Album baru ini menandai 20 tahun aku berkarya di dunia musik. Ini refleksi perjalanan musik saya dari kecil. Saya berangkat dari dunia theater. Saya juga pernah gitaris terbaik band metal se-Jabodetabek tahun 2014," kata Winky saat ditemui di Entertainment Plaza (EP) Semarang, Jumat (28/11/2014).
						<br><br>
						DJ yang bernama asli Nurayendra Irwindo ini mengisahkan, album baru bertajuk "Play List" itu melewati proses 9 bulan, mulai penyusunan materi, aransemen, rekaman, mixing, hingga pengembangan visual. Semuanya telah dirancang untuk mempresentasikan preferensi musiknya yang cukup luas dan beragam. Mulai dari gesekan gitar heavy metal hingga bebuyian beatbox hip hp bawahkan swing jazz dan bunyi psychedelic dari musik new wawf serta musik elektronk.
						<br><br>
						Aktor yang memerankan Ferdy saat film layar lebar berjudul Jelangkung ini dedikasikan CD pertama terhadap karirnya sebagai DJ. Lagu-lagu di CD pertama menunjukkan bagaimana keahlian dan perkembangan musikalitas Winky dengan musik elektronik.
						Nomor-nomor pada CD pertama akan terasa kombinasis sinkupasi, melodi, acid, dan electro dengan sentuan trap yang menghadirkan sensasi bunyi kohesif mulai dari lagi pertama sampai terakhir.
						<br><br>
						"Kalau lagu CD pertama semua isinya irama disco, pada CD kedua ada 7 lagu yang isinya lebih mirip band hasil kolaborasi kompossisi sendiri dengan pendukung sejumlah musisi. Ini berisi tentang idealismeku sebagai seorang seniman musik, kaya variasi melodi dan kisah di balik lirik yang menjelajah dimensi waktu dan suasana hati," ujar pria kelahiran Bandung, 9 Desember 1978 tersebut (*)
						<div class="map">
							<iframe class="map_full" width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=jakarta&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=50.424342,70.048828&amp;ie=UTF8&amp;hq=&amp;hnear=Jakarta,+DKI+Jakarta,+Indonesia&amp;ll=-6.211544,106.845172&amp;spn=0.062369,0.068407&amp;t=m&amp;z=14&amp;output=embed"></iframe>
						</div>
					</div>
					<!-- e:text detail -->
					<div class="share">
						<!-- <span class='st_facebook_hcount' displayText='Facebook'></span>
						<span class='st_twitter_hcount' displayText='Tweet'></span>
						<span class='st_sharethis_hcount' displayText='ShareThis'></span>
						<script type="text/javascript">var switchTo5x=true;</script>
						<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
						<script type="text/javascript">stLight.options({publisher: "75863e9d-6d43-4f36-b9f2-979bb0e94566", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
						 --><div class="sh">
							<a class="twitter-share-button"
							  href="https://twitter.com/share"
							  data-via="twitterdev">
							Tweet
							</a>
							<script>
							window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
							</script>
						</div>
						<div class="sh">
							<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>

							<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
						</div>
						<div class="sh">
							<a href="https://www.pinterest.com/pin/create/button/
							    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
							    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
							    &description=Next%20stop%3A%20Pinterest"
							    data-pin-do="buttonPin"
							    data-pin-config="above">
							    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
							</a>
							<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
						</div>
						<!-- <div class="sh">
							<a name="fb_share" type="button_count" share_url="http://www.detiksport.com/sepakbola/read/2011/08/22/104213/1708029/72/tanpa-rio-vidic-mu-tetap-kuat" href="http://www.facebook.com/sharer.php?u=http://www.detiksport.com/sepakbola/read/2011/08/22/104213/1708029/72/tanpa-rio-vidic-mu-tetap-kuat">Share</a> 
	    					<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script> 
						</div>
						<div class="sh">	
	    					<a href="http://twitter.com/share" class="twitter-share-button" data-url="http://de.tk/x26zO" data-text="Karena Salas, Juventus Gagal Dapatkan CR7" data-count="horizontal"  data-via="detiksport" data-related="detikcom"  data-counturl="http://us.detiksport.com/sepakbola/read/2011/07/11/055822/1678344/71/karena-salas-juventus-gagal-dapatkan-cr7">Tweet</a> 
	    					<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script> 
    					</div> -->
					</div>
				</article>
				
				<div class="clearfix"></div>
				<!-- s:related -->
				<div class="box_1 box_2 box_2a">
					<div class="title">
						<span>Related Article</span>
					</div>
					<div class="list_2 list_2b">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/img1.jpg"/></div>
								</div>
								<h3>BCM</h3>
							</a>
						</article>
					</div>
					<div class="list_2 list_2b">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/img2.jpg"/></div>
								</div>
								<h3>ZOUK</h3>
							</a>
						</article>
					</div>
					<div class="list_2 list_2b">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
								</div>
								<h3>JSS</h3>
							</a>
						</article>
					</div>
					<div class="list_2 list_2b">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/img4.jpg"/></div>
								</div>
								<h3>AMNESIA</h3>
							</a>
						</article>
					</div>
				</div>
				<!-- e:related -->
			</div>
			<!-- e:left	 -->
			<!-- s:right	 -->
			<div class="c_right">
				<!-- s:scratch -->
				<div class="box_1">
					<div class="title">
						<span>SCRATCH</span>
					</div>
					<ul class="list_musik">
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
					</ul>
				</div>
				<!-- e:scratch -->
				<!-- s:banner r1 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- e:banner r1 -->
				<!-- s:quiz -->
				<div class="box_1 box_2">
					<div class="title">
						<span>quiz</span>
					</div>
					<div class="quiz_box">
						<form action="#">
							<div class="q">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat, magna vitae porta scelerisque ?
							</div>
							<div class="a">
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare. 
									</div>
									<div class="clearfix"></div>
								</label>
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare consectetur adipiscing elit.  
									</div>
									<div class="clearfix"></div>
								</label>
							</div>
							<input type="submit" value="SUBMIT" class="btn_submit">
						</form>
					</div>
				</div>
				<!-- e:quiz -->
				<!-- s:banner r2 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- s:banner r2 -->
			</div>
			<!-- s:right	 -->
			<div class="clearfix"></div>
		</div>
		<!-- s:footer -->
		<?php include "includes/footer.php";?>
		<!-- e:footer -->
	</div>
	<?php include "includes/js.php";?>
</body>
</html>