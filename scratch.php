<!DOCTYPE html>
<html>
<?php $page="scratch";?>
<?php include "includes/head.php";?>
<script src="js/jwplayer.js" ></script>
<script src="js/jwplayer.html5.js" ></script>
<body>
	<?php include "includes/framebar.php";?>
	<div class="skin1">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="skin2">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="container">
		<!-- s:header -->
		<?php include "includes/header.php";?>
		<!-- e:header -->
		<div class="content">
			<!-- s:title page -->
			<div class="title_page">
				<span>scratch</span>
			</div>
			<!-- e:title page -->
			<!-- s:left	 -->
			<div class="c_left">
				<!-- s:list -->
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio1">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio1").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>

				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio2">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio2").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio3">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio3").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/img1.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio4">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio4").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/img2.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio5">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio5").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/img3.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio6">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio6").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/img5.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio7">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio7").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<div class="scr_list">
					<div class="ratio1_1 box_img">
						<div class="img_con imgLiquid"><img  src="img/img6.jpg"/></div>
					</div>
					<div class="text">
						<div class="jdl">
							<h2>DJ Yasmin  -  <span>Lagu Pertama</span></h2>
						</div>
						<div class="audio_area">
							<div id="audio8">Loading the player...</div>
							<script type="text/javascript">
							    jwplayer("audio8").setup({
							        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
							        width: "100%",
							        height: 30
							    });
							</script>
						</div>
						<div class="share">
							<div class="sh">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-via="twitterdev">
								Tweet
								</a>
								<script>
								window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
								</script>
							</div>
							<div class="sh">
								<div id="fb-root"></div>
								<script>(function(d, s, id) {
								  var js, fjs = d.getElementsByTagName(s)[0];
								  if (d.getElementById(id)) return;
								  js = d.createElement(s); js.id = id;
								  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=778051895547087";
								  fjs.parentNode.insertBefore(js, fjs);
								}(document, 'script', 'facebook-jssdk'));</script>

								<div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
							</div>
							<div class="sh">
								<a href="https://www.pinterest.com/pin/create/button/
								    ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
								    &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
								    &description=Next%20stop%3A%20Pinterest"
								    data-pin-do="buttonPin"
								    data-pin-config="above">
								    <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" />
								</a>
								<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
							</div>
							
						</div>
					</div>
				</div>
				<!-- e:list -->
				<div class="clearfix"></div>
				<div align="center" class="loadmore">
					<img src="img/loadmore.gif" alt=""><br>
					LOAD MORE..
				</div>
				<div class="btn_loadmore"><a href="#">LOAD MORE</a></div>
			</div>
			<!-- e:left	 -->
			<!-- s:right	 -->
			<div class="c_right">
				<!-- s:scratch -->
				<div class="box_1">
					<div class="title">
						<span>SCRATCH</span>
					</div>
					<ul class="list_musik">
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
					</ul>
				</div>
				<!-- e:scratch -->
				<!-- s:banner r1 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- e:banner r1 -->
				<!-- s:quiz -->
				<div class="box_1 box_2">
					<div class="title">
						<span>quiz</span>
					</div>
					<div class="quiz_box">
						<form action="#">
							<div class="q">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat, magna vitae porta scelerisque ?
							</div>
							<div class="a">
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare. 
									</div>
									<div class="clearfix"></div>
								</label>
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare consectetur adipiscing elit.  
									</div>
									<div class="clearfix"></div>
								</label>
							</div>
							<input type="submit" value="SUBMIT" class="btn_submit">
						</form>
					</div>
				</div>
				<!-- e:quiz -->
				<!-- s:banner r2 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- s:banner r2 -->
			</div>
			<!-- s:right	 -->
			<div class="clearfix"></div>
		</div>
		<!-- s:footer -->
		<?php include "includes/footer.php";?>
		<!-- e:footer -->
	</div>
	<?php include "includes/js.php";?>
</body>
</html>