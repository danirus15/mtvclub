<!DOCTYPE html>
<html>
<?php $page="home";?>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/framebar.php";?>
	<div class="skin1">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="skin2">
		<a href="#"><img src="img/banner_skin.png" alt=""></a>
	</div>
	<div class="container">
		<!-- s:header -->
		<?php include "includes/header.php";?>
		<!-- e:header -->
		<div class="content">
			<!-- s:left	 -->
			<div class="c_left">
				<!-- s:headline -->
				<div class="hl">
					<ul id="hl">
						<li>
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
									</div>
									<div class="text">
										<h1>Luncurkan Album Baru, DJ Winky Bercerita Tentang 20 Tahun</h1>
									</div>
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/img4.jpg"/></div>
									</div>
									<div class="text">
										<h1>Luncurkan Album Baru, DJ Winky Bercerita Tentang 20 Tahun</h1>
									</div>
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/img3.jpg"/></div>
									</div>
									<div class="text">
										<h1>Luncurkan Album Baru, DJ Winky Bercerita Tentang 20 Tahun</h1>
									</div>
								</a>
							</article>
						</li>
					</ul>
					<div id="pagnation" class="page_slide"></div>
				</div>
				<!-- e:headline -->
				<div class="clearfix"></div>
				<!-- s:beat -->
				<!-- <div class="box_1 box_2 w300 fl">
					<div class="title">
						<span>beats</span>
					</div>
					<div class="list_1">
						<article>
							<a href="#">
								<div class="ratio2_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/img1.jpg"/></div>
								</div>
								<h2>Jast5 Cipta Lagu dari cur- hat Rekan Segrup</h2>
							</a>
							<div class="clearfix pt10"></div>
							<div class="ico_list fl mr15">
								<img src="img/ico_time.png" alt="">
								<span>27 Feb 2015</span>
							</div>
							<div class="ico_list">
								<img src="img/ico_comment.png" alt="">
								<span>27</span>
							</div>
						</article>
					</div>
				</div> -->
				<!-- e:beat -->
				<!-- s:selector -->
				<div class="box_1 box_2">
					<div class="title">
						<span>selector</span>
					</div>
					<div>
						<div class="list_1 list_300">
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/img2.jpg"/></div>
									</div>
									<h2 class="big">DJ Al Ghazali</h2>
								</a>
								<div class="clearfix pt10"></div>
								<div class="ico_list fl mr15">
									<img src="img/ico_time.png" alt="">
									<span>27 Feb 2015</span>
								</div>
								<div class="ico_list">
									<img src="img/ico_comment.png" alt="">
									<span>27</span>
								</div>
							</article>
						</div>
						<div class="list_1 list_300">
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/img2.jpg"/></div>
									</div>
									<h2 class="big">DJ Al Ghazali</h2>
								</a>
								<div class="clearfix pt10"></div>
								<div class="ico_list fl mr15">
									<img src="img/ico_time.png" alt="">
									<span>27 Feb 2015</span>
								</div>
								<div class="ico_list">
									<img src="img/ico_comment.png" alt="">
									<span>27</span>
								</div>
							</article>
						</div>
					</div>
				</div>
				<!-- e:selector -->
				<div class="clearfix"></div>
				<!-- s:snap -->
				<div class="box_1 box_2">
					<div class="title">
						<span>snap</span>
					</div>
					<div class="list_2">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/pic1.jpg"/></div>
								</div>
								<h3>Hi Everybody, You're Watching CLUBMTV</h3>
							</a>
						</article>
					</div>
					<div class="list_2">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/pic2.jpg"/></div>
								</div>
								<h3>#ShopJeen</h3>
							</a>
						</article>
					</div>
					<div class="list_2">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/pic3.jpg"/></div>
								</div>
								<h3>#ShopJeen</h3>
							</a>
						</article>
					</div>
					<div class="list_2">
						<article>
							<a href="#">
								<div class="ratio1_1 box_img">
									<div class="img_con imgLiquid"><img  src="img/pic4.jpg"/></div>
								</div>
								<h3>Hi Everybody, You're Watching CLUBMTV</h3>
							</a>
						</article>
					</div>
				</div>
				<!-- e:snap -->
				<div class="clearfix pt20"></div>
				<!-- s:club -->
				<div class="box_1 box_2">
					<div class="title">
						<span>CLUBS</span>
					</div>
					<div>
						<div class="list_1 list_300">
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/img3.jpg"/></div>
									</div>
									<h2 class="big">Pazha Ibiza Hi Everybody, You're Watching CLUBMTV sdfas</h2>
								</a>
								<div class="clearfix pt10"></div>
								<div class="ico_list fl mr15">
									<img src="img/ico_time.png" alt="">
									<span>Jumat, 27 April 2015</span>
								</div>
								<div class="ico_list">
									<img src="img/ico_comment.png" alt="">
									<span>27</span>
								</div>
							</article>
						</div>
						<div class="list_1 list_300">
							<article>
								<a href="#">
									<div class="ratio2_1 box_img">
										<div class="img_con imgLiquid"><img  src="img/img3.jpg"/></div>
									</div>
									<h2 class="big">Pazha Ibiza</h2>
								</a>
								<div class="clearfix pt10"></div>
								<div class="ico_list fl mr15">
									<img src="img/ico_time.png" alt="">
									<span>27 Feb 2015</span>
								</div>
								<div class="ico_list">
									<img src="img/ico_comment.png" alt="">
									<span>27</span>
								</div>
							</article>
						</div>
					</div>
				</div>
				<!-- e:club -->
				
				<div class="clearfix"></div>
			</div>
			<!-- e:left	 -->
			<!-- s:right	 -->
			<div class="c_right">
				<!-- s:scratch -->
				<div class="box_1">
					<div class="title">
						<span>SCRATCH</span>
					</div>
					<ul class="list_musik">
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>Ninda Felina</h3>
										DJ Ninda Felina live @CLUBMTV The Edge Kemang, 26 April 2015
										<div class="publisher">CLUBMTVID</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
						<li>
							<article>
								<a href="#">
									<div class="pic"><img src="img/cover1.jpg" alt=""></div>
									<div class="text">
										<h3>ARGY & MAMA</h3>
										Recluse (Hot Since 82 Remix)
										<div class="publisher">Universal Music Inter.</div>
									</div>
									<img src="img/ico_play.png" alt="" class="play">
								</a>
							</article>
						</li>
					</ul>
				</div>
				<!-- e:scratch -->
				<!-- s:banner r1 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- e:banner r1 -->
				<!-- s:quiz -->
				<div class="box_1 box_2">
					<div class="title">
						<span>quiz</span>
					</div>
					<div class="quiz_box">
						<form action="#">
							<div class="q">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat, magna vitae porta scelerisque ?
							</div>
							<div class="a">
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare. 
									</div>
									<div class="clearfix"></div>
								</label>
								<label>
									<input type="radio" name="quiz">
									<div class="text">
										Praesent vestibulum luctus ornare consectetur adipiscing elit.  
									</div>
									<div class="clearfix"></div>
								</label>
							</div>
							<input type="submit" value="SUBMIT" class="btn_submit">
						</form>
					</div>
				</div>
				<!-- e:quiz -->
				<!-- s:banner r2 -->
				<div class="banner_reg">
					<a href="#"><img src="img/banner_r.jpg" alt=""></a>
				</div>
				<!-- s:banner r2 -->
			</div>
			<!-- s:right	 -->
			<div class="clearfix"></div>
		</div>
		<!-- s:footer -->
		<?php include "includes/footer.php";?>
		<!-- e:footer -->
	</div>
	<?php include "includes/js.php";?>
</body>
</html>