<script src="http://localhost/dn/awal/mtvclub/html/js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.carouFredSel-6.0.4-packed.js"></script>
<script type="text/javascript" src="js/swipe.js"></script>
<script type="text/javascript" src="js/imgLiquid-min.js"></script>
<script type="text/javascript" src="js/_controller.js"></script>
<script type="text/javascript">
    $(function() {
    
    $('#thumbs').carouFredSel({
        /*responsive: true,*/
        synchronise: ['#images', false, true],
        auto: false,
        items: {
            visible: 8,
            start: -1
        },
        scroll: {
            onBefore: function( data ) {
                data.items.old.eq(1).removeClass('selected');
                data.items.visible.eq(1).addClass('selected');
            },
            items:1
        },
        prev: '#prev',
        next: '#next'
    });

    $('#images').carouFredSel({
        responsive: true,
        auto: false,
        items: 1,
        scroll: {
            fx: 'fade',
            items:1,
            onBefore : function( data ) {
                data.items.visible.find(".text").hide();
            },
            onAfter : function( data ) { 
                data.items.visible.find(".text").fadeIn(200);
            }
        }
        
    });

    $('#thumbs div').click(function() {
        $('#thumbs').trigger( 'slideTo', [this, -1] );
    });
    $('#thumbs div:eq(1)').addClass('selected');

    
});
</script> 
<link href="css/frame.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<style>
    .wpgal {
    background-color: #000;
    width: 300px;
    position: relative;
    border-bottom: 1px solid #fff;
}
.wpgal #images {
    width: 300px;
    height: 150px;
    background:#000;
}
.wpgal #images div {
    width:300px;
    height: 150px;
    display:block;
    float:left;
    background:#000;
}
.wpgal #images div img {
    width:300px;
    height:150px;
}
.wpgal #thumbs {
    width: 320px;
    height: 37.5px;
    float:left;
    border-top: 1px solid #fff;
}
.wpgal #thumbs>div {
    float:left;
    width:36.5px;
    height:36.5px;
    text-align:center;
    overflow:hidden;
    display:block;
    border-right: 1px solid #fff;
    opacity: 0.7;
}
.wpgal #thumbs>div:hover, 
#thumbs>div.selected {
    cursor:pointer;
    opacity: 1;
}
.wpgal #thumbs .box_img {
    width: 100%;
}
.nav_gal {
    position: absolute;
    bottom: 0px;
    height: 20px;
    padding: 8px 8px;
    width: 20px;
    cursor: pointer;
    background-color: rgba(0,0,0,0.6);
    opacity: 0.7;
}
.nav_gal:hover {
    opacity: 1;
}
.nav_gal_r {
    right: 0;
}
</style>
<!--S:HEADLINE BOX-->
<div class="wpgal">
    <div id="images">
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
                </div>
            </a>
        </div>
        <div>
            <a href="#" class="snap_detail.php">
                <div class="ratio2_1 box_img">
                    <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
                </div>
            </a>
        </div>
    </div>
    
    <div id="thumbs">
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl1.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl2.jpg"/></div>
            </div>
        </div>
        <div>
            <div class="ratio1_1 box_img">
                <div class="img_con imgLiquid"><img  src="img/hl3.jpg"/></div>
            </div>
        </div>
    </div>
    <img src="img/nav2_l.png" id="prev" class="nav_gal nav_gal_l">
    <img src="img/nav2_r.png" id="next" class="nav_gal nav_gal_r">
    <div class="clearfix"></div>
</div>
<div class="clearfix pt15"></div>