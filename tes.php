<html>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>

<style>
	.galkanal #headline_fotos div .text h6 {
	font-size:12px;
	color:#aaa;
}
.galkanal #headline_fotos div .text a {
	text-decoration:none; 
	color:#fff;
	font-size:22px;
}
.galkanal #headline_fotos div .text a:hover {
	color:#FFF;
}
.galkanal #headline_fotos div img {
	height:100%;
	/*width:100%;*/
}
.galkanal .caroufredsel_wrapper {
	
}
.galkanal .thumbs {
	width: 620px;  
	height: 68px;
	position: relative; 
}
.galkanal .thumbs div {
	float:none;
	width:124px;
	height:68px;
	text-align:center;
	background:#000;
	overflow:hidden;
    display:inline-block;
	opacity: 0.4;
}
.galkanal .thumbs div:hover, 
.thumbs div.selected {
	cursor:pointer;
	opacity: 1;
	width:124px;
	height:68px;
}
.galkanal .thumbs div .pic {
	width:100%;
	height:100%;
	display:block;
	overflow:hidden;
	text-align:center;
	position:relative;
}
.galkanal .thumbs div .pic img {
	width:145px;
}
.nav_gal {
	background: rgba(252,0,0,0.7);
	position: absolute;
	top: 38%;
	display: inline-block;
	padding: 15px 8px;
	z-index: 2;
	cursor:pointer;
	
	-webkit-border-top-right-radius: 100px;
	-webkit-border-bottom-right-radius: 100px;
	-moz-border-radius-topright: 100px;
	-moz-border-radius-bottomright: 100px;
	border-top-right-radius: 100px;
	border-bottom-right-radius: 100px;
}
.nav_gal:hover { 
	background: rgba(255, 133, 0, 0.7);
}
.nav_gal_r {
	right: 0;
	-webkit-border-top-left-radius: 100px;
	-webkit-border-bottom-left-radius: 100px;
	-moz-border-radius-topleft: 100px;
	-moz-border-radius-bottomleft: 100px;
	border-top-left-radius: 100px;
	border-bottom-left-radius: 100px;
	
	-webkit-border-top-right-radius: 0px;
	-webkit-border-bottom-right-radius: 0px;
	-moz-border-radius-topright: 0px;
	-moz-border-radius-bottomright: 0px;
	border-top-right-radius: 0px;
	border-bottom-right-radius: 0px;
}
.list_gal li{
	margin:5px 8px !important;
}
</style>
<body>
	<script src="js/jquery.min.js" ></script>

	 <!--s:Foto-->
    	<div class="box">
        	<div class="sub_box5">  
            	<div class="title"> Foto   <a href="indeks.php" class="indeks">+indeks</a> </div>
                <!-- s:foto -->
                <div class="box_hl wpgal galkanal">
                	<div class="error">Artikel belum tersedia</div>
                    <div class="bigpic">
                        <div id="headline_fotos">
                            <div id="c1"  class="thumb">
                                <span class="text"> 
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Ekonomi Bisinis </h4> 
                        			<h3><a href="detail.php">  Pegawai Chevron dan PetroChina Ikut Demo Bareng Pertamina  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <a href="detail.php"><span class="pic"><img src="image/foto-landscape12.jpg"/></span></a> 
                            </div>
                            <div id="c2"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Moneter </h4> 
                        			<h3><a href="detail.php">  Seremoni Groundbreaking Lippo Thamrin  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape13.jpg"/></span> 
                            </div>
                            <div id="c3"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Bisnis </h4> 
                        			<h3><a href="detail.php">  Pameran Kerajinan UMKM  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape14.jpg"/></span> 
                            </div>
                            <div id="c4"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Ekonomi Bisinis </h4> 
                        			<h3><a href="detail.php">  Dolar AS Mendekati Rp 12.000  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape15.jpg"/></span>
                                
                            </div>
                            <div id="c5"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Perdagangan </h4> 
                        			<h3> <a href="detail.php">  Penampakan Terkini Jalur Proyek Jalan Puncak II  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape16.jpg"/></span>
                                
                            </div>
                            <div id="c6"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Ekonomi Bisinis </h4> 
                        			<h3> <a href="detail.php"> Sertijab Menko Perekonomian dari Hatta ke CT  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape17.jpg"/></span>
                                
                            </div>
                            <div id="c7"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Industri </h4> 
                        			<h3><a href="detail.php">  Beban Listrik Jawa Bali Capai Rekor Tertinggi  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape18.jpg"/></span>
                                
                            </div>
                            <div id="c8"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Industri </h4> 
                        			<h3><a href="detail.php">  Pegawai Chevron dan PetroChina Ikut Demo Bareng Pertamina  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-portrait1.jpg"/></span>
                            </div>
                            <div id="c9"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Industri </h4> 
                        			<h3><a href="detail.php">  Pegawai Chevron dan PetroChina Ikut Demo Bareng Pertamina  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape12.jpg"/></span>
                                
                            </div>
                            <div id="c10"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Industri </h4> 
                        			<h3><a href="detail.php">  Pegawai Chevron dan PetroChina Ikut Demo Bareng Pertamina  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape13.jpg"/></span>
                                
                            </div>
                            <div id="c11"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Industri </h4> 
                        			<h3><a href="detail.php">  Pegawai Chevron dan PetroChina Ikut Demo Bareng Pertamina  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape14.jpg"/></span>
                                
                            </div>
                            <div id="c12"  class="thumb">
                                <span class="text">
                                    <h6> Senin, 13/06/2011 09:18 </h6> 
                                    <h4> Industri </h4> 
                        			<h3> <a href="detail.php">  Pegawai Chevron dan PetroChina Ikut Demo Bareng Pertamina  </a></h3>  
                                    <!--Fox Searchlight saat ini sedang berusaha untuk menjangkau lebih luas penonton dengan '12 Years a Slave'. Selain mengeluarkan versi DVD pada 4 Maret mendatang, pihak studio juga akan menayangkan lagi di 1000 layar bioskop.-->
                                </span>
                                <span class="pic"><img src="image/foto-landscape15.jpg"/></span>
                            </div>
                        </div>
                    </div>
                    <div id="thumbnails" class="thumbs">
                        <div id="c1"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape12.jpg"/></span>
                        </div>
                        <div id="c2"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape13.jpg"/></span>
                        </div>
                        <div id="c3"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape14.jpg"/></span>
                        </div>
                        <div id="c4"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape15.jpg"/></span>
                        </div>
                        <div id="c5"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape16.jpg"/></span>
                        </div>
                        <div id="c6"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape17.jpg"/></span>
                        </div>
                        <div id="c7"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape18.jpg"/></span>
                        </div>
                        <div id="c8"  class="thumb">
                            <span class="pic"><img src="image/foto-portrait1.jpg"/></span>
                        </div>
                        <div id="c9"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape12.jpg"/></span>
                        </div>
                        <div id="c10"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape13.jpg"/></span>
                        </div>
                        <div id="c11"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape14.jpg"/></span>
                        </div>
                        <div id="c12"  class="thumb">
                            <span class="pic"><img src="image/foto-landscape15.jpg"/></span>
                        </div>
                       
                    </div>
                    <img src="image/nav_fot_l.png" id="prev" class="nav_gal">
                    <img src="image/nav_fot_r.png" id="next" class="nav_gal nav_gal_r">
                    <div class="clearfik"></div> 
                </div>
                <!-- e:foto -->
                <div class="clearfik"></div> 
            </div> 
            <div class="clearfik"></div>
        </div>
        <!--e:foto-->
	<script src="js/jquery.carouFredSel-6.0.4-packed.js" ></script>
	<script>
	

/* S:carouFredSel */
$(function() {    

	 /* ----- s:headline foto galeri ----- */ 
	  
	 /*big foto*/
	 $('#headline_fotos').carouFredSel({ 
		scroll: {
			items: 1,
			fx: 'crossfade',
			pauseOnHover: "true"
		},  
		
		next: {
				//button: '.hl_foto_next2',
				button: '#next', 
				onBefore: function( oldI, newI ) { 
					  $('#thumbnails div.thumb').trigger( 'slideTo', [ '#thumbnails div[id='+ newI.attr( 'id' ) +']', -1 ] ); 
					  $('#thumbnails div.thumb').removeClass('selected');
					  $('#thumbnails div[id='+ newI.attr( 'id' ) +']').addClass('selected'); 
			  }
		},
		
		prev: {
				//button: '.hl_foto_prev2',
				button: '#prev', 
				onBefore: function( oldI, newI ) { 
					  $('#thumbnails div.thumb').trigger( 'slideTo', [ '#thumbnails div[id='+ newI.attr( 'id' ) +']', -1 ] ); 
					  $('#thumbnails div.thumb').removeClass('selected');
					  $('#thumbnails div[id='+ newI.attr( 'id' ) +']').addClass('selected'); 
			  }
		},
		
		auto: {
				play: false,
				onBefore: function( oldI, newI ) { 
					  $('#thumbnails div.thumb').trigger( 'slideTo', [ '#thumbnails div[id='+ newI.attr( 'id' ) +']', -1 ] ); 
					  $('#thumbnails div.thumb').removeClass('selected');
					  $('#thumbnails div[id='+ newI.attr( 'id' ) +']').addClass('selected'); 
			}
		}
		
	}); 
	
    /*thumbs foto*/
	$('#thumbnails').carouFredSel({ 
			auto: false, 
			items: {
				start: -1
			}, 
			next: '.hl_foto_next',
			prev: '.hl_foto_prev'  
    });
	
	/*trigger slide*/ 
	$('#thumbnails div.thumb').click(function() {
		$('#thumbnails div.thumb').trigger( 'slideTo', [ this, -1 ] );  
		
		$('#headline_fotos div.thumb').trigger( 'slideTo', [ '#headline_fotos div[id='+ $(this).attr( 'id' ) +']' ] );
		
		$('#thumbnails div.thumb').removeClass('selected');
		$(this).addClass('selected');
	    return false; 
	})  
	 
	 
	/* ----- e:headline foto galeri ----- */
	
	
    $('#thumbs').carouFredSel({
		synchronise: ['#images', false, true],
		auto: false,
		items: {
			visible: 8,
			start: -1
		},
		scroll: {
			onBefore: function( data ) {
				data.items.old.eq(1).removeClass('selected');
				data.items.visible.eq(1).addClass('selected');
			},
			items:1
		},
		prev: '#prev',
		next: '#next'
	});
	
	/*s:Foto gallery*/

	$('#thumbs div').click(function() {
		$('#thumbs').trigger( 'slideTo', [this, -1] );
	});
	$('#thumbs div:eq(1)').addClass('selected');

	
});

/* E:carouFredSel */
</script>
	

</body>
</html>