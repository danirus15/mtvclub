<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
<link rel="shortcut icon" href="img/favicon.gif">
<link rel="apple-touch-icon" href="img/favicon.gif"/> 
<title>MTV Club</title>
<link href="css/fonts.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/frame.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="css/microsite.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link rel="shortcut icon" href="img/favicon.gif">
</head>
<body>
	<div class="microsite">
		<img src="img/logo_big.png" alt="" class="logo_cover">
		<br>
		GET YOUR CHANCE TO BE INVITED<br>
		<a href="microsite_reg.php">REGISTER HERE!</a>
	</div>
</body>
</html>