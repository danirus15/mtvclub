<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
	<link rel="shortcut icon" href="img/favicon.gif">
	<link rel="apple-touch-icon" href="img/favicon.gif"/> 
	<title>MTV Club</title>
	<link href="css/fonts.css" rel="stylesheet" type="text/css" charset="utf-8"/>
	<link href="css/frame.style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
	<link href="css/style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
	<link href="css/style.responsive.css" rel="stylesheet" type="text/css" charset="utf-8"/>
	<link rel="shortcut icon" href="img/favicon.gif">
	<script src="js/jquery.min.js" ></script>
	<script src="js/jwplayer.js" ></script>
	<script src="js/jwplayer.html5.js" ></script>
</head>
<body class="pop_body">
	<div class="pop_bg close_box_in"></div>
	<div class="video_container">
		<span class="close_box_in close_box_style">x</span>
		<h1>#DJ ODEX perform</h1>
		<div class="ico_list">
			<img src="img/ico_time.png" alt="">
			<span>27 Feb 2015</span>
		</div>
		<div class="clearfix pt20"></div>
		<div class="video_area">
			<div id="mtvvideo">Loading the player...</div>
			<script type="text/javascript">
			    jwplayer("mtvvideo").setup({
			        file: "http://content.jwplatform.com/videos/HkauGhRi-640.mp4",
			        image: "img/video1.jpg",
			        width: "100%",
		      		aspectratio: "16:9"
			    });
			</script>
		</div>
		<div class="clearfix pt20"></div>
		<div class="nav2">
			<a href="#"><img src="img/nav_l.png" alt=""></a>
			<a href="#"><img src="img/nav_r.png" alt=""></a>
		</div>
		<div class="share fr">
			<a href="#">
				<img src="img/ico_fb.png" alt="">
				<span>Facebook</span>
			</a>
			<a href="#">
				<img src="img/ico_tw.png" alt="">
				<span>Twitter</span>
			</a>
			<a href="#">
				<img src="img/ico_pin.png" alt="">
				<span>Pinterest</span>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<script type="text/javascript" src="js/_controller.js"></script>
</body>
</html>